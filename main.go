package main

import (
	"net/http"
	"os"
	"os/exec"

	"github.com/valyala/fasthttp"
)

type handler struct {
	cmd *exec.Cmd
}

func (h *handler) ServeHTTP(wr http.ResponseWriter, req *http.Request) {
	output, err := h.cmd.Output()
	if nil != err {
		wr.Write([]byte(err.Error()))
		wr.WriteHeader(500)
		return
	}

	wr.Write(output)
	wr.WriteHeader(fasthttp.StatusOK)
}

func main() {
	cmd := exec.Command("/opt/vc/bin/vcgencmd", "measure_temp")

	h := handler{cmd}

	http.ListenAndServe("0.0.0.0:"+os.Args[1], &h)
}
